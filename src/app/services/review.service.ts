import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class ReviewService {

  constructor(
    private http: HttpClient
  ) { }

  getReviews(params) {
    const apiUrl = environment.apiUrl + 'reviews';
    return this.http.get(apiUrl, {
      params: params
    });
  }

  addReview(review) {
    const apiUrl = environment.apiUrl + 'reviews';
    return this.http.post(apiUrl, review);
  }

  deleteReview(review) {
    const apiUrl = environment.apiUrl + 'reviews/' + review.id;
    return this.http.delete(review)
  }

  editReview(review) {
    const apiUrl = environment.apiUrl + 'reviews/' + review.id;
    return this.http.put(apiUrl, review);
  }
}
