import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class CourseService {

  constructor(
    private http: HttpClient
  ) { }

  getCourses() {
    const apiUrl = environment.apiUrl + 'courses';
    return this.http.get(apiUrl);
  }

  addCourse(course) {
    const apiUrl = environment.apiUrl + 'courses';
    return this.http.post(apiUrl, course);
  }

  deleteCourse(course) {
    const apiUrl = environment.apiUrl + 'courses/' + course.id;
    return this.http.delete(apiUrl)
  }

  editCourse(course) {
    const apiUrl = environment.apiUrl + 'courses/' + course.id;
    return this.http.put(apiUrl, course);
  }

}
