import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class ProfessorService {

  constructor(
    private http: HttpClient
  ) { }

  getProfessors() {
    const apiUrl = environment.apiUrl + 'professors';
    return this.http.get(apiUrl);
  }

  addProfessor(professor) {
    const apiUrl = environment.apiUrl + 'professors';
    return this.http.post(apiUrl, professor);
  }

  deleteProfessor(professor) {
    const apiUrl = environment.apiUrl + 'professors/' + professor.id;
    return this.http.delete(apiUrl)
  }

  editProfessor(professor) {
    const apiUrl = environment.apiUrl + 'professors/' + professor.id;
    return this.http.put(apiUrl, professor);
  }

}
