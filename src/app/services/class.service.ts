import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class ClassService {

  constructor(
    private http: HttpClient
  ) { }

  getClass(id) {
    const apiUrl = environment.apiUrl + 'professor_classes/' + id;
    return this.http.get(apiUrl);
  }

  getClasses() {
    const apiUrl = environment.apiUrl + 'professor_classes';
    return this.http.get(apiUrl);
  }

  addClass(classObj) {
    const apiUrl = environment.apiUrl + 'professor_classes';
    return this.http.post(apiUrl, classObj);
  }

  deleteClass(classObj) {
    const apiUrl = environment.apiUrl + 'professor_classes/' + classObj.id;
    return this.http.delete(apiUrl)
  }

  addProfessor(obj) {
    const apiUrl = environment.apiUrl + 'professor_classes_professors/';
    return this.http.post(apiUrl, obj);
  }

  getProfessors(params) {
    const apiUrl = environment.apiUrl + 'professor_classes_professors/';
    return this.http.get(apiUrl, {params: params});
  }


}
