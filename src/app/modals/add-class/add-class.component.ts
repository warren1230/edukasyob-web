import { Component, OnInit } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ClassService } from '../../services/class.service';
import { CourseService } from '../../services/course.service';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';

@Component({
  selector: 'app-add-class',
  templateUrl: './add-class.component.html',
  styleUrls: ['./add-class.component.css']
})
export class AddClassComponent implements OnInit {

  classObj:any = {};
  courses: any = [];
  isError:boolean = false;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.courses.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );
  formatter = (x: {name: string }) => x.name;

  constructor(
    private activeModal: NgbActiveModal,
    private classService: ClassService,
    private courseService: CourseService
  ) { }

  ngOnInit() {
    this.getCourses();
  }

  addClass(classObj) {
    if(!classObj.name.name) {
      this.isError = true;
    } else {
      classObj = {
        name: classObj.name.name
      }
      this.classService.addClass(classObj).subscribe( (response) => {
        this.activeModal.close();
      })
    }
  }

  getCourses() {
    this.courseService.getCourses().subscribe( (response) => {
      this.courses = response;
    })
  }

}
