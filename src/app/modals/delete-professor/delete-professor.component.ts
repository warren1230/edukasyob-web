import { Component, OnInit } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ProfessorService } from '../../services/professor.service';

@Component({
  selector: 'app-delete-professor',
  templateUrl: './delete-professor.component.html',
  styleUrls: ['./delete-professor.component.css']
})
export class DeleteProfessorComponent implements OnInit {

  constructor(
    private activeModal: NgbActiveModal,
    private professorService: ProfessorService
  ) { }

  ngOnInit() {
  }

  deleteProfessor() {
    this.professorService.deleteProfessor(this['professorObj']).subscribe( (response) => {
      this.activeModal.close();
    })
  }

}
