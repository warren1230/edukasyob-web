import { Component, OnInit } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { CourseService } from '../../services/course.service';

@Component({
  selector: 'app-delete-course',
  templateUrl: './delete-course.component.html',
  styleUrls: ['./delete-course.component.css']
})
export class DeleteCourseComponent implements OnInit {

  constructor(
    private activeModal: NgbActiveModal,
    private courseService: CourseService
  ) { }

  ngOnInit() {
  }

  deleteCourse() {
    this.courseService.deleteCourse(this['courseObj']).subscribe( (response) => {
      this.activeModal.close();
    })
  }

}
