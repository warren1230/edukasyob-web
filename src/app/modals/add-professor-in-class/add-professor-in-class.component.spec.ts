import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProfessorInClassComponent } from './add-professor-in-class.component';

describe('AddProfessorInClassComponent', () => {
  let component: AddProfessorInClassComponent;
  let fixture: ComponentFixture<AddProfessorInClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddProfessorInClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProfessorInClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
