import { Component, OnInit } from '@angular/core';

import { ProfessorService } from '../../services/professor.service';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ClassService} from '../../services/class.service';

@Component({
  selector: 'app-add-professor-in-class',
  templateUrl: './add-professor-in-class.component.html',
  styleUrls: ['./add-professor-in-class.component.css']
})
export class AddProfessorInClassComponent implements OnInit {

  professorObj = {};
  professors:any = [];
  obj = {};

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.professors.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );
  formatter = (x: {name: string }) => x.name;
  isError:boolean = false;

  constructor(
    private activeModal: NgbActiveModal,
    private professorService: ProfessorService,
    private classService: ClassService
  ) { }

  ngOnInit() {
    this.getProfessors();
  }

  getProfessors() {
    this.professorService.getProfessors().subscribe((response) => {
      this.professors = response;
    });
  }

  addProfessor(professorObj) {
    if(!professorObj.id) {
      this.isError = true;
    } else {
      this.obj = {
        professor_id: professorObj.id,
        professor_class_id: this['classObj']['id']
      }
      this.classService.addProfessor(this.obj).subscribe( (response) => {
        this.activeModal.close();
      })
    }
  }

}
