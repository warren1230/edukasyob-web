import { Component, OnInit } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ProfessorService } from '../../services/professor.service';

@Component({
  selector: 'app-add-professor',
  templateUrl: './add-professor.component.html',
  styleUrls: ['./add-professor.component.css']
})
export class AddProfessorComponent implements OnInit {

  professor = {};
  buttonName = '';
  isError: boolean = false;

  constructor(
    private activeModal: NgbActiveModal,
    private professorService: ProfessorService
  ) { }

  ngOnInit() {
    if(this['professorObj']) {
      this.buttonName = 'Edit Professor'
      this.professor = this['professorObj'];
    } else {
      this.buttonName = 'Add Professor';
    }
  }

  addProfessor(professor) {
    if(!professor.name) {
      this.isError = true;
    } else {
      let professorObj = {
        name: professor.name,
        professor_class_id: ''
      }
      this.professorService.addProfessor(professorObj).subscribe( (response) => {
        this.activeModal.close();
      })
    }
  }

  editProfessor(professor) {
    if(!professor.name) {
      this.isError = true;
    } else {
      let professorObj = {
        name: professor.name,
        professor_class_id: ''
      }
      this.professorService.editProfessor(professorObj).subscribe( (response) => {
        this.activeModal.close();
      })
    }
  }

  execute(professor) {
    if(this['professorObj']) {
      this.editProfessor(professor);
    } else {
      this.addProfessor(professor);
    }
  }

}
