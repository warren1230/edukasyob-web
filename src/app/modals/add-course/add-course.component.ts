import { Component, OnInit } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { CourseService } from '../../services/course.service';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.css']
})
export class AddCourseComponent implements OnInit {

  course = {};
  buttonName = '';

  constructor(
    private activeModal: NgbActiveModal,
    private courseService: CourseService
  ) { }

  ngOnInit() {
    if(this['courseObj']) {
      this.buttonName = 'Edit Course'
      this.course = this['courseObj'];
    } else {
      this.buttonName = 'Add Course';
    }
  }

  addCourse(course) {
    this.courseService.addCourse(course).subscribe( (response) => {
      this.activeModal.close();
    })
  }

  editCourse(course) {
    console.log('course', course);
    this.courseService.editCourse(course).subscribe( (response) => {
      this.activeModal.close();
    })
  }

  execute(course) {
    if(this['courseObj']) {
      this.editCourse(course);
    } else {
      this.addCourse(course);
    }
  }

}
