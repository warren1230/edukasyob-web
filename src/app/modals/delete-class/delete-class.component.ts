import { Component, OnInit } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ClassService } from '../../services/class.service';

@Component({
  selector: 'app-delete-class',
  templateUrl: './delete-class.component.html',
  styleUrls: ['./delete-class.component.css']
})
export class DeleteClassComponent implements OnInit {

  constructor(
    private activeModal: NgbActiveModal,
    private classService: ClassService
  ) { }

  ngOnInit() {
  }

  deleteClass() {
    this.classService.deleteClass(this['classObj']).subscribe( () => {
      this.activeModal.close();
    })
  }

}
