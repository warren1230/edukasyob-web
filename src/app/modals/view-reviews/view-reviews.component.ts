import { Component, OnInit } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ReviewService } from '../../services/review.service';

@Component({
  selector: 'app-view-reviews',
  templateUrl: './view-reviews.component.html',
  styleUrls: ['./view-reviews.component.css']
})
export class ViewReviewsComponent implements OnInit {

  reviews: any = [];

  constructor(
    private activeModal: NgbActiveModal,
    private reviewService: ReviewService
  ) { }

  ngOnInit() {
    this.getReviews(this['professor']);
  }

  getReviews(professor) {
    let params = {
      professors_id: professor.id
    };
    this.reviewService.getReviews(params).subscribe( (response) => {
      this.reviews = response;
    });
  }

}
