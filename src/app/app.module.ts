import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import { FormsModule } from '@angular/forms';


import { Routes, RouterModule, Router } from '@angular/router';


import { AppComponent } from './app.component';
import { ClassesComponent } from './feature/classes/classes.component';
import { ProfessorsComponent } from './feature/professors/professors.component';
import { RateProfessorComponent } from './feature/rate-professor/rate-professor.component';
import { ManageCoursesComponent } from './feature/manage-courses/manage-courses.component';
import { ManageProfessorsComponent } from './feature/manage-professors/manage-professors.component';
import { ManageClassesComponent } from './feature/manage-classes/manage-classes.component';

import { CourseService } from './services/course.service';
import { ProfessorService } from "./services/professor.service";
import { ClassService } from './services/class.service';
import { ReviewService } from './services/review.service';

import { AddCourseComponent } from './modals/add-course/add-course.component';
import { DeleteCourseComponent } from './modals/delete-course/delete-course.component';
import { AddProfessorComponent } from './modals/add-professor/add-professor.component';
import { DeleteProfessorComponent } from './modals/delete-professor/delete-professor.component';
import { AddClassComponent } from './modals/add-class/add-class.component';
import { DeleteClassComponent } from './modals/delete-class/delete-class.component';
import { AddProfessorInClassComponent } from './modals/add-professor-in-class/add-professor-in-class.component';
import { ViewReviewsComponent } from './modals/view-reviews/view-reviews.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/classes', pathMatch: 'full'},
  {path: 'classes', component: ClassesComponent},
  {path: 'classes/:id/professors', component: ProfessorsComponent},
  {path: 'manage-courses', component: ManageCoursesComponent},
  {path: 'manage-professors', component: ManageProfessorsComponent},
  {path: 'manage-classes', component: ManageClassesComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    ClassesComponent,
    ProfessorsComponent,
    RateProfessorComponent,
    ManageCoursesComponent,
    ManageProfessorsComponent,
    ManageClassesComponent,
    AddCourseComponent,
    DeleteCourseComponent,
    AddProfessorComponent,
    DeleteProfessorComponent,
    AddClassComponent,
    DeleteClassComponent,
    AddProfessorInClassComponent,
    ViewReviewsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    )
  ],
  entryComponents: [
    RateProfessorComponent,
    AddCourseComponent,
    DeleteCourseComponent,
    AddProfessorComponent,
    DeleteProfessorComponent,
    AddClassComponent,
    DeleteClassComponent,
    AddProfessorInClassComponent,
    ViewReviewsComponent
  ],
  providers: [
    CourseService,
    ProfessorService,
    ClassService,
    ReviewService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
