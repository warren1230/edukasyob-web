import { Component, OnInit } from '@angular/core';
import { CourseService } from '../../services/course.service';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { AddCourseComponent } from '../../modals/add-course/add-course.component';
import { DeleteCourseComponent } from '../../modals/delete-course/delete-course.component';

@Component({
  selector: 'app-manage-courses',
  templateUrl: './manage-courses.component.html',
  styleUrls: ['./manage-courses.component.css']
})
export class ManageCoursesComponent implements OnInit {

  courses:any = [];

  constructor(
    private courseService: CourseService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getCourses();
  }

  getCourses() {
    this.courseService.getCourses().subscribe( (response) => {
      this.courses = response;
    });
  }

  editCourse(course) {
    const modal = this.modalService.open(AddCourseComponent);
    modal.componentInstance.courseObj = course;
    modal.result.then( () => {
      this.getCourses();
    });
  }

  deleteCourse(course) {
    const modal = this.modalService.open(DeleteCourseComponent);
    modal.componentInstance.courseObj = course;
    modal.result.then( () => {
      this.getCourses();
    });
  }

  addCourse() {
    this.modalService.open(AddCourseComponent).result.then( () => {
      this.getCourses();
    });
  }

}
