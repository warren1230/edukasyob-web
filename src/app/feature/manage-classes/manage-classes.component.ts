import { Component, OnInit } from '@angular/core';

import { ClassService } from '../../services/class.service';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { AddClassComponent } from '../../modals/add-class/add-class.component';
import { DeleteClassComponent } from '../../modals/delete-class/delete-class.component';
import { AddProfessorInClassComponent } from '../../modals/add-professor-in-class/add-professor-in-class.component';
// import { DeleteProfessorComponent } from '../../modals/delete-professor/delete-professor.component';

@Component({
  selector: 'app-manage-classes',
  templateUrl: './manage-classes.component.html',
  styleUrls: ['./manage-classes.component.css']
})
export class ManageClassesComponent implements OnInit {

  classes: any = [];

  constructor(
    private modalService: NgbModal,
    private classService: ClassService
  ) { }

  ngOnInit() {
    this.getClasses();
  }

  getClasses() {
    this.classService.getClasses().subscribe( (response) => {
      this.classes = response;
    });
  }

  deleteClass(classObj) {
    const modal = this.modalService.open(DeleteClassComponent);
    modal.componentInstance.classObj = classObj;
    modal.result.then( () => {
      this.getClasses();
    });
  }

  addClass() {
    this.modalService.open(AddClassComponent).result.then( () => {
      this.getClasses();
    });
  }

  addProfessor(classObj) {
    const modal = this.modalService.open(AddProfessorInClassComponent);
    modal.componentInstance.classObj = classObj;
  }

}
