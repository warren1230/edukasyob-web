import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { ClassService } from '../../services/class.service';

@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.css']
})
export class ClassesComponent implements OnInit {

  classes: any = []

  constructor(
    private classService: ClassService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getClasses();
  }

  goToProfessors(id) {
    this.router.navigate(['classes/' + id + '/professors']);
  }

  getClasses() {
    this.classService.getClasses().subscribe( (response) => {
      this.classes = response;
    })
  }

}
