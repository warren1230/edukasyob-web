import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';

import { ClassService } from '../../services/class.service';

import { RateProfessorComponent } from '../rate-professor/rate-professor.component';
import { ViewReviewsComponent } from '../../modals/view-reviews/view-reviews.component';

@Component({
  selector: 'app-professors',
  templateUrl: './professors.component.html',
  styleUrls: ['./professors.component.css'],
  entryComponents: [
    RateProfessorComponent
  ]
})
export class ProfessorsComponent implements OnInit {

  professors:any = [];
  className = '';

  constructor(
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private classService: ClassService
  ) {
    this.route.params.subscribe(params => {
      this.getProfessors(params['id']);
      this.getClass(params['id']);
    })
   }

  ngOnInit() {
  }

  getClass(id) {
    this.classService.getClass(id).subscribe((response) => {
      this.className = response['name'];
    });
  }

  getProfessors(classId) {
    let params = {
      professor_class_id: classId
    };

    this.classService.getProfessors(params).subscribe( (response) => {
      this.professors = response;
    })
  }

  rateProfessor(professor) {
    const modal = this.modalService.open(RateProfessorComponent);
    modal.componentInstance.professor = professor;
    modal.result.then(() => {
      this.route.params.subscribe(params => {
        this.getProfessors(params['id']);
      })
    }); 
  }

  viewReviews(professor) {
    const modal = this.modalService.open(ViewReviewsComponent);
    modal.componentInstance.professor = professor;
    modal.result.then(() => {
      this.route.params.subscribe(params => {
        this.getProfessors(params['id']);
      })
    }); 
  }

}
