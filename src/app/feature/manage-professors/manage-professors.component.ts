import { Component, OnInit } from '@angular/core';

import { ProfessorService } from '../../services/professor.service';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { AddProfessorComponent } from '../../modals/add-professor/add-professor.component';
import { DeleteProfessorComponent } from '../../modals/delete-professor/delete-professor.component';

@Component({
  selector: 'app-manage-professors',
  templateUrl: './manage-professors.component.html',
  styleUrls: ['./manage-professors.component.css']
})
export class ManageProfessorsComponent implements OnInit {

  professors: any = [];

  constructor(
    private modalService: NgbModal,
    private professorService: ProfessorService
  ) { }

  ngOnInit() {
    this.getProfessors();
  }

  getProfessors() {
    this.professorService.getProfessors().subscribe( (response) => {
      this.professors = response;
    });
  }

  editProfessor(professor) {
    const modal = this.modalService.open(AddProfessorComponent);
    modal.componentInstance.professorObj = professor;
    modal.result.then( () => {
      this.getProfessors();
    });
  }

  deleteProfessor(professor) {
    const modal = this.modalService.open(DeleteProfessorComponent);
    modal.componentInstance.professorObj = professor;
    modal.result.then( () => {
      this.getProfessors();
    });
  }

  addProfessor() {
    this.modalService.open(AddProfessorComponent).result.then( () => {
      this.getProfessors();
    });
  }
}
