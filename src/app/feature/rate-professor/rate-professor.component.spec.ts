import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateProfessorComponent } from './rate-professor.component';

describe('RateProfessorComponent', () => {
  let component: RateProfessorComponent;
  let fixture: ComponentFixture<RateProfessorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateProfessorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateProfessorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
