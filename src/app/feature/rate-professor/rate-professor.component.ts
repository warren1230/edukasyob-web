import { Component, OnInit } from '@angular/core';

import { ReviewService } from '../../services/review.service';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-rate-professor',
  templateUrl: './rate-professor.component.html',
  styleUrls: ['./rate-professor.component.css']
})
export class RateProfessorComponent implements OnInit {

  review = {};

  constructor(
    private reviewService: ReviewService,
    private activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
    console.log('this.professor', this['professor']);
  }

  rateProfessor(review) {
    let reviewObj = {
      professors_id: this['professor']['id'],
      rating: parseInt(review.rating),
      description: review.description
    };
    this.reviewService.addReview(reviewObj).subscribe( (response) => {
      this.activeModal.close();
    })
  }

}
